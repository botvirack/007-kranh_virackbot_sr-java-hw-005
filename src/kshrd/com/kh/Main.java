package kshrd.com.kh;
import java.util.Scanner;
public class Main implements Runnable {
    public static final String ANSI_BLUE = "\u001B[34m";
    public static final String ANSI_PURPLE = "\u001B[35m";
    public static final String YELLOW_BOLD_BRIGHT = "\033[1;93m";
    String msg;
    Main(String msg) {
        this.msg = msg;
    }
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        boolean exist = false;
        do {
            System.out.print(YELLOW_BOLD_BRIGHT + "->" + ANSI_BLUE + "    Greeting  : " + ANSI_PURPLE);
            String s = sc.nextLine();
            if (s.equalsIgnoreCase("Java Greeting")) {
                try {
                    System.out.println();
                    Thread t1 = new Thread(new Main("\t\tHello KSHRD!"));
                    t1.start();
                    t1.join();
                    System.out.println();
                    Thread t2 = new Thread(new Main("************************************"));
                    t2.start();
                    t2.join();
                    System.out.println();
                    Thread t3 = new Thread(new Main("I will try my best to be here at HRD."));
                    t3.start();
                    t3.join();
                    System.out.println();
                    Thread t4 = new Thread(new Main("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"));
                    t4.start();
                    t4.join();
                    System.out.println();
                    System.out.print("Downloading");
                    Thread t5 = new Thread(new Main("..........."));
                    t5.start();
                    t5.join();
                    System.out.print("Completed 100%");
                    System.out.println();
                } catch (Exception e) {
                    System.out.println(e);
                }
            } else if (s.equals("stop")) {
                exist = true;
            } else {
                System.out.println("Error: Could not find or load main class greeting");
            }
        } while (exist == false);
    }
    public void run() {
        try {

            char[] chars = msg.toCharArray();

            for (int i = 0; i < chars.length; i++) {
                System.out.print(chars[i]);
                Thread.sleep(250);
            }

        } catch (Exception e) {
            System.out.println(e);
        }
    }
}
